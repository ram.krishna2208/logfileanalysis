#!/bin/bash


logfile=out.log
RandomError=`cat $logfile   | grep -i "RandomError"   | wc -l` 
ValueError=`cat $logfile    | grep -i "ValueError"    | wc -l`
VariableError=`cat $logfile | grep -i "VariableError" | wc -l`
UnknownError=`cat $logfile  | grep -i "UnknownError"  | wc -l`
RunTimeError=`cat $logfile  | grep -i "RunTimeError"  | wc -l`

echo "*******histogram of the errors raised**************" > /tmp/log.out

echo "RandomError: $RandomError, ValueError: $ValueError, VariableError: $VariableError, UnknownError: $UnknownError, RunTimeError : $RunTimeError" >> /tmp/log.out
cat out.log |  sed  's/[][]//g;s/ms//g' | grep -i "Everything looks good" | cut -d" " -f 5 | sort > /tmp/count

echo "*******50 percentile, 90 percentile, 95 percentile stats about the response time of successful*********" >> /tmp/log.out

a=`cat /tmp/count | wc -l`
echo $a

for i in 0.50 0.90 0.95
	do
	a=$(echo "$a*$i" | bc)
	echo $a | grep "^[0-9]*$"
	cond=`echo $?`
		if [ $cond == 0 ]
			then
			d=`expr $a + 1`
			e=`sed -n $d /tmp/count`
			c=`sed -n $a /tmp/count`
			f=`expr $e + $c`
			f=`expr $f\2`
		else
			b=`printf '%.*f\n' 0 $a`
			j=$( echo "$i" |cut -d\. -f2 )
			echo  "$j percentile stats about the response time of successful responses  is $b" >> /tmp/log.out
		fi
done
	
Response=`cat json.jq | jq  '.rules[] | {Responsetime, Numberoferrors}'  | grep -i "response" | cut -d":" -f2 | cut -d"\"" -f2 | sed 's/ms//g' | cut -d" " -f1`

echo "*************Response time > 100ms for more than 5 seconds***************" >> /tmp/log.out
count=1
echo $Response
echo $Numberoferrors
number=1
sec=-1
add=`sed -n 1p out.log | cut -d" " -f1-2 | cut -d"." -f1`

if [ -z $Response ]; then
	echo "not triggred from json file"
else
	echo $Response
	line=`cat out.log | wc -l`
		while [ $count -le $line ]
		do
			for i in {1..5}
			do
			date=`date -d "$add $sec sec ago" "+%Y-%m-%d %H:%M:%S"`
			responcse_status=`sed -n "$count"p out.log | grep ms | cut -d" " -f1-5 | sed 's/[][]//g;s/ms//g'`  
			search=`sed -n "$count"p out.log | grep ms | cut -d" " -f1-2`
			count=`expr $count + 1`
            sec=`expr $sec + 1`
			response_ms=`echo $responcse_status | awk {'print $5'}`
			echo $response_ms
			  	if [ "$response_ms" -le 100 ] ; then
					echo "resonse time is more than 100ms"
					cat out.log | grep "$search" >> /tmp/log.out
				fi
				sec=`expr $sec + 1`	
			done
		done 

fi

echo "***************Number of errors > 5 in 1 minute interval*****************" >> /tmp/log.out
Numberoferrors=` cat json.jq | jq  '.rules[] | {Responsetime, Numberoferrors}'  | grep -i "number" | cut -d":" -f2 | cut -d"\"" -f2 | sed 's/ms//g' | cut -d" " -f1`
count=1
number=1
find=1
sec=1
add=`sed -n 1p out.log | cut -d" " -f1-2 | cut -d"." -f1`

if ! [ -z  $Numberoferrors ]; then
	echo $Numberoferrors
    line=`cat out.log | wc -l`
    while [ $count -le $line ]
    do
	date=`date -d "$add $sec sec ago" "+%Y-%m-%d %H:%M:%S"`
    cat out.log | grep  "$date" >> /tmp/Error
    sed -i '/^\s*$/d' /tmp/Error
    Error_status=`cat /tmp/Error | wc -l`
        while [ $Error_status -eq 60 ]
        do
        pattern=`cat /tmp/Error | grep -i "Error" | wc -l`
            if [ $pattern -ge 5 ]; then
                cat /tmp/Error | grep "Error" >> /tmp/log.out
                cat /dev/null > /tmp/Error
            fi
                Error_status=`cat /tmp/Error | wc -l`
        done
    sec=`expr $sec + 1`
    count=`expr $count + 1`
	done
 fi














